from pymongo import MongoClient
from eve import Eve

# MONGODB
client = MongoClient()
db = client.apiexercise  # use a database called "apiexercise"

# REST API
api_settings = {
    'MONGO_HOST' : 'localhost',
    'MONGO_PORT' : 27017,

    # Skip these if your db has no auth. But it really should.
    #'MONGO_USERNAME' : ,
    #'MONGO_PASSWORD' : ,
    #'MONGO_AUTH_SOURCE' : 'admin',  # needed if --auth mode is enabled

    'MONGO_DBNAME' : 'apiexercise',
    'RESOURCE_METHODS' : ['GET', 'POST', 'DELETE'],
    'ITEM_METHODS' : ['GET', 'PATCH', 'DELETE', 'PUT'],
    'EXTENDED_MEDIA_INFO' : ['content_type', 'name', 'length'],
    'RETURN_MEDIA_AS_BASE64_STRING' : False,
    'RETURN_MEDIA_AS_URL': True,
    'CACHE_CONTROL' : 'max-age=20',
    'CACHE_EXPIRES' : 20,
    'ALLOW_UNKNOWN' : True,
    'DOMAIN' : {'people': {
          'schema':
              {
                  '_id' : {
                      'type': 'integer',
                  },
                  'Survived' : {
                      'type': 'integer',
                  },
                  'Pclass' : {
                      'type': 'integer',
                  },
                  'Name': {
                      'type': 'string',
                  },
                  'Sex' : {
                      'type': 'string',
                  },
                  'Age' : {
                      'type': 'integer',
                  },
                  'SiblingsOrSpousesAboard"' : {
                      'type': 'integer',
                  },
                  'ParentsOrChildrenAboard' : {
                      'type': 'integer',
                  },
                  'Fare' : {
                      'type': 'integer',
                  }
              }
          }
        }
}

app = Eve(settings=api_settings)
app.run()